#!/bin/bash
function local_app() {
    ansible-playbook --connection=local gitlab-runner/local.yml --ask-become-pass    
}
function remote_app() {
    cd gitlab-runner
    terraform init
    terraform validate
    terraform apply -auto-approve
}
case "$1" in 
    local)   local_app ;;
    remote)    remote_app ;;
    *) echo "usage: $0 local|remote" >&2
       exit 1
       ;;
esac