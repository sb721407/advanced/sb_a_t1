##### backend group #####

resource "yandex_alb_backend_group" "alb-bg2" {
  name = "alb-bg2"
  http_backend {
    name             = "backend-2"
    port             = 8080
    target_group_ids = [yandex_alb_target_group.alb-tg2.id]
    healthcheck {
      timeout          = "10s"
      interval         = "2s"
      healthcheck_port = 8080
      http_healthcheck {
        path = "/"
      }
    }
  }
}

##### http router Domain name akimov-petr.mooo.com #####

resource "yandex_alb_http_router" "alb-router2" {
  name = "alb-router2"
}
resource "yandex_alb_virtual_host" "alb-host2" {
  name           = "alb-host2"
  http_router_id = yandex_alb_http_router.alb-router2.id
  authority      = ["lb-akimov.mooo.com"]
  route {
    name = "route-2"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.alb-bg2.id
      }
    }
  }
}

##### Application Load Balancer ######

resource "yandex_alb_load_balancer" "alb-2" {
  name               = "alb-2"
  network_id         = yandex_vpc_network.network-2.id
  security_group_ids = [yandex_vpc_security_group.alb-vm-sg2.id, yandex_vpc_security_group.alb-sg2.id]
  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.subnet-2.id
    }

  }

  listener {
    name = "alb-listener2"
    endpoint {
      address {
        external_ipv4_address {
          address = "62.84.118.134"
        }
      }
      ports = [8080]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.alb-router2.id
      }
    }
  }
}
