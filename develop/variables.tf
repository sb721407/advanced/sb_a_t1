variable "ssh_credentials" {
  description = "Credentials for connect to instances"
  type = object({
    user        = string
    private_key = string
    pub_key     = string
  })
  default = {
    user        = "ubuntu"
    private_key = "ansible.key"
    pub_key     = "~/.ssh/id_rsa.pub"
  }
}

#variable "elastic_cluster" {
#  description = "Elasticsearch host name"
#  type = string
#  default = yandex_mdb_elasticsearch_cluster.my-es-clstr2.host.[1].fqdn
#}
