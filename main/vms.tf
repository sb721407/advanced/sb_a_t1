##### VMs Part #####
resource "yandex_compute_instance" "monitoring" {
  name        = "monitoring"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd87ap2ld09bjiotu5v0" # ubuntu-20
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "service1" {
  name        = "service1"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd87ap2ld09bjiotu5v0" # ubuntu-20
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "service2" {
  name        = "service2"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd87ap2ld09bjiotu5v0" # ubuntu-20
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }

  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "service3" {
  name        = "service3"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }

  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd87ap2ld09bjiotu5v0" # ubuntu-20
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

##### ALB Target Group #####

resource "yandex_alb_target_group" "alb-tg" {
  name = "alb-tg"

  target {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = yandex_compute_instance.service1.network_interface.0.ip_address
  }
  target {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = yandex_compute_instance.service2.network_interface.0.ip_address
  }
  target {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = yandex_compute_instance.service3.network_interface.0.ip_address
  }
}

##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[service]
${yandex_compute_instance.service1.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'
${yandex_compute_instance.service2.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'
${yandex_compute_instance.service3.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[monitoring]
${yandex_compute_instance.monitoring.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[consul_instances]
${yandex_compute_instance.monitoring.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=ansible.key consul_bind_address=${yandex_compute_instance.monitoring.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.monitoring.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.service1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=ansible.key consul_bind_address=${yandex_compute_instance.service1.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.service1.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true
${yandex_compute_instance.service2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=ansible.key consul_bind_address=${yandex_compute_instance.service2.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.service2.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true
${yandex_compute_instance.service3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=ansible.key consul_bind_address=${yandex_compute_instance.service3.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.service3.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true

EOF
  filename = "${path.module}/inventory"
}

##### Provisioning #####

resource "null_resource" "service1" {
  depends_on = [yandex_compute_instance.service1, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.service1.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
}

resource "null_resource" "service2" {
  depends_on = [yandex_compute_instance.service2, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.service2.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
}

resource "null_resource" "service3" {
  depends_on = [yandex_compute_instance.service3, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.service3.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
}

resource "null_resource" "monitoring" {
  depends_on = [yandex_compute_instance.monitoring, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
}

#resource "null_resource" "consul" {
#  depends_on = [null_resource.service1, null_resource.service2, null_resource.service3, null_resource.monitoring]
#  provisioner "local-exec" {
#    command = "ansible-playbook -u ubuntu -i inventory --key-file ansible.key consul.yml"
#  }
#}

#resource "null_resource" "site" {
#  depends_on = [null_resource.consul]
#  provisioner "local-exec" {
#    command = "ansible-playbook -u ubuntu -i inventory --key-file ansible.key site.yml"
#  }
#}

##### Provisioning2 #####

resource "null_resource" "server" {
  depends_on = [null_resource.monitoring]
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ansible.key monitoring.yml"
  }
}

##### Provisioning3 #####

resource "null_resource" "clients" {
  depends_on = [null_resource.service1, null_resource.service2, null_resource.service3]
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ansible.key pb.yml"
  }
}

##### Provisioning4 #####

resource "null_resource" "site" {
  depends_on = [null_resource.server, null_resource.clients]
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ansible.key site.yml"
  }
}


##### ALB IP-Address and DNS A-record #####

resource "yandex_vpc_address" "addr" {
  name = "static-ip"
  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}

resource "yandex_dns_recordset" "server_dns_name" {
  depends_on = [yandex_vpc_address.addr]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "alb1"
  type       = "A"
  ttl        = 200
  data       = [yandex_vpc_address.addr.external_ipv4_address[0].address]
}

##### Monitoring DNS A-record #####

resource "yandex_dns_recordset" "monitoring_dns_name" {
  depends_on = [yandex_compute_instance.monitoring]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "mon1"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
}

##### Services 1-3 DNS A-records #####
resource "yandex_dns_recordset" "srv1_dns_name" {
  depends_on = [yandex_compute_instance.service1]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "service1"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.service1.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "srv2_dns_name" {
  depends_on = [yandex_compute_instance.service2]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "service2"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.service2.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "srv3_dns_name" {
  depends_on = [yandex_compute_instance.service3]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "service3"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.service3.network_interface.0.nat_ip_address]
}
